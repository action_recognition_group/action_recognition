action_regognition
==============================

A short description of the project.

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

# Формируем подвыборку датасета Kinetics 700-2020

## Запуск скачивания csv файлов

В репозитории ([ссылка](https://github.com/cvdfoundation/kinetics-dataset)) есть ссылки на скачивания csv-файлов датасета `Kinetics-700-2020`:

Они понадобятся нам для скачивания подвыборки этого датасета.

Выполним команду:
```
python src/data/kinetics_csv_download.py
```

## Запуск скачивания датасета kinetics

С помощью `src/data/kinetics_download_cfg_create.py` можно создать конфигурационные файлы для скачивания желаемой подвыборки датасета.

Используемые нами параметры приведены в данной папке:
```
src/data/download_cfg
├── cfg_train.json
└── cfg_val.json
```

Используем их для скачивания подвыборки Kinetics 700-2020 видео с классами содержащими слово `dancing`:
* train (не больше 200 видео на каждый класс)
* val (не больше 50 видео на каждый класс)

Для этого выполним команды:
```
python src/data/kinetics_download.py --cfg src/data/download_cfg/cfg_train.json
python src/data/kinetics_download.py --cfg src/data/download_cfg/cfg_val.json
```

Получим датасет:
```
data/interim/kinetics_700_2020
├── train
│   ├── kinetics_download_log_2024-04-15_01:49:49.log
│   ├── train.csv
│   ├── train_i_last.csv
│   └── videos
└── val
    ├── kinetics_download_log_2024-04-22_04:50:17.log
    ├── val.csv
    ├── val_i_last.csv
    └── videos
```

## Создадим train, val, test

Исходная test выборка датасета не содержит правильный `label`.
Было принято следующее решение:
* разбить исходную выборку `train` на `train`(80%) и `val`(20%)

для этого выполним команду:
```
python src/data/kinetics_train_val_split.py
```

В результате
```
data/interim/kinetics_700_2020
├── train
│   ├── train.csv
```

разделиться на
```
data/interim/kinetics_700_2020
├── train
│   ├── train_part.csv (80% исходной выборки)
│   ├── val_part.csv (20% исходной выборки)
```

* выборку `val` использовать как `test`
```
data/interim/kinetics_700_2020
└── val
    ├── kinetics_download_log_2024-04-22_04:50:17.log
    ├── val.csv
    ├── val_i_last.csv
    └── videos
```

## Разведочный анализ данных полученного датасета

Для полученного датасета производился разведочных анализ данных(`EDA`).

Анализировалось распределение видео по классам и продолжительность видео:
```
notebooks
├── eda_src_dataset.ipynb (анализ исходных 'train' и 'val' частей датсета `Kinetics 700 2020`)
└── eda_new_dataset_download.ipynb (анализ полученных 'train', 'val' и 'test' частей для обучения и тестирования)
```

# 3.1 Классификатор кадров видео

Мы хотим обучить модель на отдельных кадрах видео и предсказывать класс видео как наиболее часто встречаемый среди классов для его кадров.
Такая модель имеет информацию только с текущего кадра видео.
В качестве модели классифицирующей кадры была выбрана модель, - `yolov8m-cls`([ссылка](https://docs.ultralytics.com/tasks/classify/)).

## Запуск создания датасета из кадров видео

С помощью `src/data/slice_videos_to_frame_cfg_create.py` можно создать конфигурационные файлы для желаемого создания датасета для классификации кадров.

Используемые нами параметры приведены в данной папке:
```
src/data/slice_videos_cfg
├── cfg_test.json
├── cfg_train.json
└── cfg_val.json
```

Для формирования датасета из кадров `Kinetics 700-2020` выполним команды:
```
python src/data/slice_videos_to_frames.py --cfg src/data/slice_videos_cfg/cfg_train.json
python src/data/slice_videos_to_frames.py --cfg src/data/slice_videos_cfg/cfg_val.json
python src/data/slice_videos_to_frames.py --cfg src/data/slice_videos_cfg/cfg_test.json
```

Получим датасет в формате нужном для обучения `YOLOv8-cls`([ссылка](https://docs.ultralytics.com/datasets/classify/)):
```
data/processed/kinetics_700_2020_cls
├── train (101666 images in 15 classes)
│   ├── belly dancing
│   ├── breakdancing
...
│   └── tap dancing
└── val (25024 images in 15 classes)
    ├── belly dancing
    ├── breakdancing
...
    └── tap dancing
└── test (30754 images in 15 classes)
    ├── belly dancing
    ├── breakdancing
...
    └── tap dancing
```

## Обучаем классификатор

Обучим модель `yolov8m-cls` для классификации кадров из видео по классам танцев.
```
python src/models/frame_classifier/train_frame_classification.py
```

Инференс-класс `YOLOv8FrameClassifierInference` для предсказания моделью на картинках и видео представлен в `src/models/frame_classifier/frame_classifier_inference.py`.

### Результаты

Оценим метрики классификации видео моделью `frame_classifier`.
```
python src/models/frame_classifier/evaluate_frame_classifier.py
```

Полученный результат представлен в `reports/frame_classifier_results.csv`.

| model_name  | accuracy           | recall             | f1                  |
|-------------|--------------------|--------------------|---------------------|
| yolov8m-cls | 0.4341880341880342 | 0.4369608176198162 | 0.43802418582321434 |


# 3.1 Классификатор видео на основе pose estimation и tracking

Мы хотим научится:
* выделять одного танцующего человека на видео
* отслеживать его tracking'ом
* оценивать его ключевые точки

обучить модель на отдельных кадрах видео и предсказывать класс видео как наиболее часто встречаемый среди классов для его кадров.
Такая модель имеет информацию только с текущего кадра видео.
В качестве модели классифицирующей кадры была выбрана модель, - `yolov8m-pose`([ссылка](https://docs.ultralytics.com/tasks/classify/)).

Рассмотрим схему ключевых точек используемую `yolov8m-pose`.
Запустим скрипт `src/models/pose_estimation/pose_estimation_keypoint_viz.py`и получим визуализацию ключевых точек на мужчине в костюме:

![image](data/output/pose_estimation/keypoints_map/keypoints_map.jpg "YOLOv8-pose keypoint map")

## Эвристики для выделения одного человека в кадре

Далее с помощью скрипта `src/models/pose_estimation/pose_tracker_inference.py` мы визуализировали предсказания от треккера `yolov8m-pose`.

Когда `yolov8m-pose` не может найти на человеке ключевую точку,  она приписывает данной точке координаты (0,0).

Для теста мы выбрали различные видео содержащие различные трудности.
Мы собрали данные видео с визуализацией предсказаний в отдельный плейлист на YouTube(ссылка)
* "video__1e38sSEhi4.mp4" - парень с девушкой кружат в танце и переодически перекрывают друг друга.
* "video___KgrOyZBlQ.mp4"
* "video__1tHSvXN7BM.mp4"
* "video__6d4WxnukRU.mp4"
* "video__39KVssPNP4.mp4"
* "video__fGEM_oExVU.mp4"
* "video__GQvowFzEgA.mp4"
* "video_xihIy0IQn9I.mp4"
* "video_ZGwhXRnMdAA.mp4"

Мы решили взять следующие эвристики:
* детектруем людей с id их треков.
* отбираем среди детекций тех людей у которых видна (точка 5 или точка 6) и (точка 11 или точка 12)
* сортируем всех оставшихся людей по высоте бокса и затем по количеству точек
* забираем одного самого лучшего
* запоминаем себе его трек id, если человек с таким же трек id будет на следующем кадре мы сразу берем его bbox и точки, продолжая следить за ним.

## Система координат относительно человека

Когда мы забираем ключевые точки от `yolov8m-pose`, они представляют собой абсолютные координаты пикселей кадра видео.

Возможные проблемы:
* нет устойчивости к трансляции по кадру изображения.

Если мы возьмем одного и того же человека в одной и той же позе и получим для него координаты ключевых точек, они будут разными.

Мы хотим сделать локальную систему координат привязанную к каждому человеку, которого мы отслеживаем.

В качестве точки отсчета, для такой локальной системы координат берется точка центра торса человека.
Точка центра торса человека берется усреднением ненулевых ключевых точек 5,6, 11 и 12.

Данная точка показана желтым и имеет номер 18.

![image](repo_pics/center_point_viz.jpg "center point")

* хочется нормировать координаты на рост человека

Человек может быть близко к камере или на удалении от нее. 

Рост человека постоянен и не меняется, все координаты будут меньше 1.0.

Но человек может быть в кадре не целиком! Зато мы всегда отбираем тех людей у когов кадре точно находятся точки 5,6, 11,12.
Т.е. у нас точно будет высота торса человека.

Согласно пропорциям человека.

![image](repo_pics/human_proportions.jpg "center point")

Торс человека оставляет 3 головы человека, а полный рост человека, - 8 голов.
Т.е. можно нормировать на 3 высоты торса человека.

На всякий случай мы нормируем все координаты на 5 высот торса человека в кадре.

Могут быть искажения, или человек может согнуться. Мы подстраховались, чтобы координаты относительнолокальнйо системы координат точно были меньше 1.0.

## Генерация датасета

Генерируем датасет с помощью скрипта `src/models/pose_estimation/keypoint_dataset_gen.py`

При этом у нас создается следующий датасет.

# Сводная таблица результатов

| model_name            | accuracy | precision | recall | f1   |
|-----------------------|----------|-----------|--------|------|
| yolov8m-cls           | 0.43     | 0.45      | 0.44   | 0.44 |
| LSTM keypoints        | 0.33     | 0.29      | 0.31   | 0.29 |
| Transformer keypoints | 0.2      | 0.19      | 0.18   | 0.17 |
| 3D CNN                | 0.11     | 0.04      | 0.11   | 0.06 |

