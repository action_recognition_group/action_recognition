import logging
import os
import sys
from datetime import datetime

from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

sys.path.append("src/models/3d_cnn/pytorchvideo")


import numpy as np
import pandas as pd
import torch
import torch.nn as nn
from pytorchvideo.data.clip_sampling import make_clip_sampler
from pytorchvideo.data.labeled_video_dataset import labeled_video_dataset
from pytorchvideo.transforms.transforms import (
    ApplyTransformToKey,
    Normalize,
    RandomShortSideScale,
    UniformTemporalSubsample,
)
from torch.utils.data import DataLoader
from torchvision.transforms import Compose, Lambda, RandomHorizontalFlip
from torchvision.transforms._transforms_video import CenterCropVideo


def create_logger(logger_name, log_file_path, mode="w"):
    """Create a logger with a file handler.

    This function sets up a logger with a file handler that formats log messages
    to include the timestamp and the log message itself. The logger
    is configured to log messages with a severity level of INFO or higher.

    Parameters
    ----------
    logger_name : str
        The name of the logger. This is used to retrieve or create a logger
        instance.
    log_file_path : str
        The path to the log file where the log messages will be written.

    Returns
    -------
    logger : logging.Logger
        A logger instance configured with the specified name and file handler.
    """
    logger = logging.getLogger(logger_name)

    file_handler = logging.FileHandler(log_file_path, mode)
    formatter = logging.Formatter("%(asctime)s - %(message)s")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    logger.setLevel(logging.INFO)
    return logger


def predict_model(model, test_loader, device):
    model.eval()
    with torch.no_grad():
        preds, labels = [], []
        for i, batch in enumerate(test_loader):
            inputs, target = batch["video"].to(device), batch["label"].to(device)

            outputs = model(inputs)
            _, predicted = torch.max(outputs.data, 1)
            preds.extend(predicted.cpu().tolist())
            labels.extend(target.cpu().tolist())

        preds = np.array(preds)
        labels = np.array(labels)

    return preds, labels


def evaluate_metrics(preds, targets):
    # считаем метрики
    acc = accuracy_score(targets, preds)
    recall = recall_score(targets, preds, average="macro")
    precision = precision_score(targets, preds, average="macro")
    f1 = f1_score(targets, preds, average="macro")

    return acc, recall, precision, f1


class Simple3DCNN(nn.Module):
    def __init__(self, num_classes):
        super(Simple3DCNN, self).__init__()

        # First Convolutional Layer
        self.conv1 = nn.Conv3d(
            in_channels=3,
            out_channels=32,
            kernel_size=(3, 3, 3),
            stride=(1, 1, 1),
            padding=(1, 1, 1),
        )
        self.bn1 = nn.BatchNorm3d(32)
        self.relu1 = nn.ReLU()

        # Second Convolutional Layer
        self.conv2 = nn.Conv3d(
            in_channels=32,
            out_channels=64,
            kernel_size=(3, 3, 3),
            stride=(1, 1, 1),
            padding=(1, 1, 1),
        )
        self.bn2 = nn.BatchNorm3d(64)
        self.relu2 = nn.ReLU()

        # Third Convolutional Layer
        self.conv3 = nn.Conv3d(
            in_channels=64,
            out_channels=128,
            kernel_size=(3, 3, 3),
            stride=(1, 1, 1),
            padding=(1, 1, 1),
        )
        self.bn3 = nn.BatchNorm3d(128)
        self.relu3 = nn.ReLU()

        # 3D Pooling Layers
        self.pool1 = nn.MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2))
        self.pool2 = nn.MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2))
        self.pool3 = nn.MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2))

        # Fully Connected Layer
        self.fc = nn.Linear(301056, num_classes)

    def forward(self, x):
        # First Convolutional Layer
        x = self.bn1(self.relu1(self.conv1(x)))
        x = self.pool1(x)

        # Second Convolutional Layer
        x = self.bn2(self.relu2(self.conv2(x)))
        x = self.pool2(x)

        # Third Convolutional Layer
        x = self.bn3(self.relu3(self.conv3(x)))
        x = self.pool3(x)

        # Flatten the tensor for the fully connected layer
        x = x.view(x.size(0), -1)

        # Fully Connected Layer
        x = self.fc(x)

        return x


if __name__ == "__main__":
    train_csv_path = "data/processed/kinetics_700_2020_3dcnn/train_20.csv"
    val_csv_path = "data/processed/kinetics_700_2020_3dcnn/val_5.csv"
    test_csv_path = "data/processed/kinetics_700_2020_3dcnn/test_5.csv"
    logger_name = "3D CNN train"
    result_csv_path = "reports/3d_cnn_results.csv"
    model_save_dir = "models/3d_cnn"

    # параметры модели
    num_classes = 15
    clip_time = 2
    clips_per_clip = 24

    # параметры обучения
    num_epochs = 15
    batch_size = 16
    lr = 0.001

    os.makedirs(model_save_dir, exist_ok=True)

    start_time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    log_file_path = f"reports/3d_cnn_train_{start_time}.log"
    logger = create_logger(logger_name, log_file_path, "w")

    df_train = pd.read_csv(train_csv_path, sep=",", index_col=False)

    video_transform = Compose(
        [
            ApplyTransformToKey(
                key="video",
                transform=Compose(
                    [
                        UniformTemporalSubsample(clips_per_clip),
                        Lambda(lambda x: x / 255),
                        Normalize((0.45, 0.45, 0.45), (0.225, 0.225, 0.225)),
                        RandomShortSideScale(min_size=248, max_size=256),
                        CenterCropVideo(224),
                        RandomHorizontalFlip(p=0.5),
                    ]
                ),
            ),
        ]
    )

    train_dataset = labeled_video_dataset(
        train_csv_path,
        clip_sampler=make_clip_sampler("random", clip_time),
        transform=video_transform,
        decode_audio=False,
    )

    val_dataset = labeled_video_dataset(
        val_csv_path,
        clip_sampler=make_clip_sampler("random", clip_time),
        transform=video_transform,
        decode_audio=False,
    )

    test_dataset = labeled_video_dataset(
        test_csv_path,
        clip_sampler=make_clip_sampler("random", clip_time),
        transform=video_transform,
        decode_audio=False,
    )

    train_loader = DataLoader(train_dataset, batch_size, num_workers=0, pin_memory=True)
    val_loader = DataLoader(val_dataset, batch_size, num_workers=0, pin_memory=True)
    test_loader = DataLoader(test_dataset, batch_size, num_workers=0, pin_memory=True)

    # batch = next(iter(loader))
    # print(batch.keys())
    # print(batch['video'].shape, batch['label'].shape)
    # dict_keys(['video', 'video_name', 'video_index', 'clip_index', 'aug_index', 'label'])
    # torch.Size([16, 3, 24, 224, 224]) torch.Size([5])

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = Simple3DCNN(num_classes).to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr)

    message = f"Start training..."
    print(message)
    logger.info(message)

    best_f1, best_epoch = -1.0, -1

    for epoch in range(num_epochs):
        running_loss = 0.0
        dataset_len = 0
        for i, batch in enumerate(train_loader):
            inputs, labels = batch["video"].to(device), batch["label"].to(device)
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            dataset_len += len(inputs)
            print(f"Train batch {i}")

        message = f"Epoch {epoch}, Loss: {running_loss / dataset_len}"
        print(message)
        logger.info(message)

        preds, labels = predict_model(model, val_loader, device)
        acc, recall, precision, f1 = evaluate_metrics(preds, labels)
        message = f"Validation set acc:{acc:.4f}, recall:{recall:.4f}, precision:{precision:.4f}, f1:{f1:.4f}"
        print(message)
        logger.info(message)

        if f1 > best_f1:
            best_f1 = f1
            best_epoch = epoch
            model_path = f"3d_cnn_{epoch}.pt"
            model_path = os.path.join(model_save_dir, model_path)
            torch.save(model, model_path)
            message = f"New best model. Saving it to {model_path}"
            print(message)
            logger.info(message)

    # Load best model
    if best_epoch > 0:
        message = f"Validation set acc:{acc:.4f}, recall:{recall:.4f}, precision:{precision:.4f}, f1:{f1:.4f}"
        model_path = f"3d_cnn_{best_epoch}.pt"
        model_path = os.path.join(model_save_dir, model_path)
        model = torch.load(model_path).to(device)
        model.eval()
        message = f"Loading best model for testing from {model_path}"
        print(message)
        logger.info(message)

    # Test model
    preds, labels = predict_model(model, test_loader, device)
    acc, recall, precision, f1 = evaluate_metrics(preds, labels)
    message = f"Test set acc:{acc:.4f}, recall:{recall:.4f}, precision:{precision:.4f}, f1:{f1:.4f}"
    print(message)
    logger.info(message)

    dct_results = {
        "model_name": ["3D CNN"],
        "accuracy": [acc],
        "precision": [precision],
        "recall": [recall],
        "f1": [f1],
    }

    df_results = pd.DataFrame.from_dict(dct_results)
    df_results.to_csv(result_csv_path, index=False)

    message = f"End training..."
    print(message)
    logger.info(message)
