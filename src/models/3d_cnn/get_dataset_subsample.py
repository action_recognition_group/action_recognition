import pandas as pd

if __name__ == "__main__":
    csv_path = "data/processed/kinetics_700_2020_3dcnn/val_above_fps12.csv"
    samples_per_class = 5
    output_csv_path = f"data/processed/kinetics_700_2020_3dcnn/test_{samples_per_class}.csv"
    
    df = pd.read_csv(csv_path, sep=' ', header=None)
    grouped = df.groupby(1)

    # For each group, select the first samples_per_class rows
    samples_df = pd.concat([group.head(samples_per_class) for _, group in grouped], ignore_index=True)
    samples_df.to_csv(output_csv_path, index=False, sep=' ', header=False)
    
