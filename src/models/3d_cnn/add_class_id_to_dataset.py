import json
import os

import pandas as pd


def read_json(json_path):
    with open(json_path, "r") as file:
        data = json.load(file)

    return data


def get_label_id_curried(names_to_ids):
    def get_label_id(label):
        label_id = names_to_ids[label]
        return label_id

    return get_label_id


if __name__ == "__main__":
    dataset_csv_path = "data/interim/kinetics_700_2020/val/val_above_fps12.csv"
    names_to_ids_json_path = "src/models/frame_classifier/names_to_ids.json"
    dataset_3dcnn_dir = "data/processed/kinetics_700_2020_3dcnn"

    os.makedirs(dataset_3dcnn_dir, exist_ok=True)
    names_to_ids = read_json(names_to_ids_json_path)

    df = pd.read_csv(dataset_csv_path, sep=",", index_col=False)
    df["label_id"] = df["label"].apply(get_label_id_curried(names_to_ids))

    df.drop(
        ["label", "duration_cv", "duration_moviepy", "frames_cv", "fps_cv"],
        axis=1,
        inplace=True,
    )
    df = df.rename(columns={"label_id": "label", "name_video": "file"})

    result_csv_path = os.path.split(dataset_csv_path)[-1]
    result_csv_path = os.path.join(dataset_3dcnn_dir, result_csv_path)
    df.to_csv(result_csv_path, index=False, sep=' ', header=False)
