from ultralytics import YOLO


if __name__ == "__main__":
    # Load a model
    model = YOLO("yolov8m-cls.pt")  # load a pretrained model (recommended for training)

    # Train the model
    path_to_dataset = "data/processed/kinetics_700_2020_cls"
    results = model.train(
        data=path_to_dataset,
        pretrained=True,
        epochs=40,
        close_mosaic=0,
        patience=10,
        batch=16,
        imgsz=640,
        workers=12,
        project="models/frame_classifier",
    )
