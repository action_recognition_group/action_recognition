import os

import cv2
import numpy as np
import tqdm
from PIL import Image, ImageDraw, ImageFont
from ultralytics import YOLO


class YOLOv8FrameClassifierInference:
    def __init__(self, model_path, font_path):
        self.model = YOLO(model_path, task="classify")
        self.font = font_path

        # настройки визуализации
        self.labels_color = (255, 255, 255)

    def predict_on_image(self, img):
        results = self.model(img)
        pred_class_idx = results[0].probs.top1
        pred_conf = results[0].probs.top1conf.item()
        pred_class_name = self.model.names[pred_class_idx]

        return pred_class_idx, pred_conf, pred_class_name
    
    def get_names_to_ids(self):
        # переводим названия классов в их id
        names_to_ids = {v: k for k, v in self.model.names.items()}
        return names_to_ids

    def predict_on_image_and_viz(self, img):
        class_idx, conf, class_name = self.predict_on_image(img)

        if os.path.isfile(img):
            img = cv2.imread(img)

        # печатаем надпись с названием класс в рамке
        color = self.labels_color[::-1]
        text = f"Класс {class_name}({class_idx}) {conf:.2%}"
        text_coords = (0, 0)
        img = self._cv2_img_add_text(
            img,
            text,
            self.font,
            text_coords,
            color,
            text_size=30,
            backgrond=True,
        )

        return img

    @staticmethod
    def set_offset(bbox, offset):
        bbox = [
            bbox[x] - offset if x < 2 else bbox[x] + offset for x in range(len(bbox))
        ]
        return bbox

    def _cv2_img_add_text(
        self,
        pil_img,
        text,
        font,
        left_corner,
        text_rgb_color,
        text_size=24,
        backgrond=True,
        **option,
    ):
        """
        cv2_img_add_text(img, '中文', (0, 0), text_rgb_color=(0, 255, 0), text_size=12, font='mingliu.ttc')
        """
        # https://stackoverflow.com/questions/50854235/
        # how-to-draw-chinese-text-on-the-image-using-cv2-puttextcorrectly-pythonopen
        if isinstance(pil_img, np.ndarray):
            pil_img = Image.fromarray(cv2.cvtColor(pil_img, cv2.COLOR_BGR2RGB))

        draw = ImageDraw.Draw(pil_img, "RGBA")
        font_text = ImageFont.truetype(
            font=font, size=text_size, encoding=option.get("encoding", "utf-8")
        )

        # рисуем полупрозрачную подложку
        if backgrond:
            bbox = draw.textbbox(left_corner, text, font=font_text)
            bbox = self.set_offset(bbox, offset=10)
            self._current_text_obj_bbox = bbox
            draw.rectangle(bbox, fill=(38, 38, 38, 170))

        draw.text(left_corner, text, text_rgb_color, font=font_text)
        cv2_img = cv2.cvtColor(np.asarray(pil_img), cv2.COLOR_RGB2BGR)

        return cv2_img

    @staticmethod
    def _runOnVideo(video, maxFrames):
        """Генератор кадров из видео. Продолжает генерировать кадры
        пока не достигнуто количество кадров maxFrames.
        """

        readFrames = 0
        while True:
            hasFrame, frame = video.read()
            if not hasFrame:
                break

            yield frame

            readFrames += 1
            if readFrames > maxFrames:
                break

    def predict_on_video(self, path_video):
        video = cv2.VideoCapture(path_video)
        num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))

        video_frames_gen = self._runOnVideo(video, num_frames)

        # ищем самый частый класс у кадров видео
        dct_class_freq = {}
        class_count, most_freq_class_id = 0, ""
        for frame in tqdm.tqdm(video_frames_gen, total=num_frames):
            class_idx = self.predict_on_image(frame)[0]
            dct_class_freq[class_idx] = dct_class_freq.get(class_idx, 0) + 1

            if dct_class_freq[class_idx] >= class_count:
                class_count, most_freq_class_id = dct_class_freq[class_idx], class_idx

        class_occurence = class_count / num_frames
        most_freq_class = self.model.names[most_freq_class_id]

        return most_freq_class_id, class_occurence, most_freq_class

    def predict_on_video_and_viz(self, path_video, path_out_video, num_frames=None):
        # Extract video properties
        video = cv2.VideoCapture(path_video)
        width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        frames_per_second = video.get(cv2.CAP_PROP_FPS)

        if num_frames is None:
            num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))

        # Initialize video writer
        fourcc = cv2.VideoWriter_fourcc(*"MJPG")

        video_writer = cv2.VideoWriter(
            path_out_video,
            fourcc=fourcc,
            fps=float(frames_per_second),
            frameSize=(width, height),
            isColor=True,
        )

        # Enumerate the frames of the video
        video_frames_gen = self._runOnVideo(video, num_frames)
        for frame in tqdm.tqdm(video_frames_gen, total=num_frames):
            frame = self.predict_on_image_and_viz(frame)

            # Write to video file
            video_writer.write(frame)

        video.release()
        video_writer.release()


if __name__ == "__main__":
    image_path = "data/processed/kinetics_700_2020_cls/val/square dancing/video__GQvowFzEgA_frame_0.jpg"
    video_path = "data/interim/kinetics_700_2020/train/videos/video__GQvowFzEgA.mp4"

    model_path = "models/frame_classifier/train2/weights/best.pt"
    model_font_path = "models/fonts/PTSans-Regular.ttf"

    frame_classifier = YOLOv8FrameClassifierInference(model_path, model_font_path)

    # Предсказать на картинке и визуализировать предсказания
    image = frame_classifier.predict_on_image_and_viz(image_path)
    cv2.imwrite("test.jpg", image)

    # frame_classifier.predict_on_video_and_viz(video_path, "test.mp4", num_frames=None)
    print(frame_classifier.predict_on_video(video_path))
