import json

import pandas as pd
from frame_classifier_inference import YOLOv8FrameClassifierInference
from sklearn.metrics import (accuracy_score, f1_score, precision_score,
                             recall_score)

if __name__ == "__main__":
    model_path = "models/frame_classifier/train2/weights/best.pt"
    model_font_path = "models/fonts/PTSans-Regular.ttf"
    dataset_eval_csv_path = "data/interim/kinetics_700_2020/val/val.csv"
    result_csv_path = "reports/frame_classifier_results.csv"

    frame_classifier = YOLOv8FrameClassifierInference(model_path, model_font_path)
    df = pd.read_csv(dataset_eval_csv_path)
    
    # переводим названия классов в их id
    names_to_ids = frame_classifier.get_names_to_ids()
    
    # Open a file in write mode and write the dictionary to it
    with open("src/models/frame_classifier/names_to_ids.json", "w") as outfile:
        json.dump(names_to_ids, outfile, indent=4)
    
    df["class_id"] = df["label"].map(names_to_ids)

    preds = []
    for video_path in df.name_video:
        pred_class = frame_classifier.predict_on_video(video_path)[0]
        preds.append(pred_class)

    targets = df.class_id.to_numpy()
    
    # считаем метрики и сохраняем в csv
    acc = accuracy_score(targets, preds)
    recall = recall_score(targets, preds, average="macro")
    precision = precision_score(targets, preds, average="macro")
    f1 = f1_score(targets, preds, average="macro")

    dct_results = {
        "model_name": ["yolov8m-cls"],
        "accuracy": [acc],
        "precision":[precision],
        "recall": [recall],
        "f1": [f1],
    }

    df_results = pd.DataFrame.from_dict(dct_results)
    df_results.to_csv(result_csv_path, index=False)
