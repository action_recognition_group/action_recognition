import pandas as pd
import torch
import torch.optim as optim
from sequences_models import (
    LSTMClassifier,
    TimeSeriesDataset,
    evaluate_metrics,
    predict_model,
    read_dataset,
)
from torch.utils.data import DataLoader

if __name__ == "__main__":
    path_to_train_csv = "data/processed/pose_estimation/keypoint_features_train.csv"
    path_to_val_csv = "data/processed/pose_estimation/keypoint_features_val.csv"
    path_to_test_csv = "data/processed/pose_estimation/keypoint_features_test.csv"
    result_csv_path = "reports/frame_lstm_keypoints_results.csv"

    # параметры LSTM
    input_size = 34
    hidden_size = 50
    num_layers = 2
    num_classes = 15

    # параметры обучения
    num_epochs = 1000
    batch_size = 64
    lr = 0.0001

    train_data, train_labels = read_dataset(path_to_train_csv)
    val_data, val_labels = read_dataset(path_to_val_csv)
    test_data, test_labels = read_dataset(path_to_test_csv)

    train_dataset = TimeSeriesDataset(train_data, train_labels)
    train_loader = DataLoader(train_dataset, batch_size, shuffle=True)
    val_dataset = TimeSeriesDataset(val_data, val_labels)
    val_loader = DataLoader(val_dataset, batch_size, shuffle=False)
    test_dataset = TimeSeriesDataset(test_data, test_labels)
    test_loader = DataLoader(test_dataset, batch_size, shuffle=False)

    model = LSTMClassifier(input_size, hidden_size, num_layers, num_classes)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr)

    device = torch.device("cuda")
    model = model.to(device)

    for epoch in range(num_epochs):
        model.train()
        for batch_idx, (data, target) in enumerate(train_loader):
            optimizer.zero_grad()
            output = model(data.to(device))
            loss = criterion(output, target.to(device))
            loss.backward()
            optimizer.step()

        # Print loss for every epoch
        print(f"Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}")

        preds, labels = predict_model(model, val_loader, device)
        acc, recall, precision, f1 = evaluate_metrics(preds, labels)
        print(
            f"Validation set acc:{acc:.4f}, recall:{recall:.4f}, precision:{precision:.4f}, f1:{f1:.4f}"
        )

    preds, labels = predict_model(model, val_loader, device)
    acc, recall, precision, f1 = evaluate_metrics(preds, labels)
    print(
        f"Test set acc:{acc:.4f}, recall:{recall:.4f}, precision:{precision:.4f}, f1:{f1:.4f}"
    )

    dct_results = {
        "model_name": ["LSTM keypoints"],
        "accuracy": [acc],
        "precision": [precision],
        "recall": [recall],
        "f1": [f1],
    }

    df_results = pd.DataFrame.from_dict(dct_results)
    df_results.to_csv(result_csv_path, index=False)
