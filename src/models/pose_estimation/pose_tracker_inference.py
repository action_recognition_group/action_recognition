import os

import cv2
import numpy as np
import tqdm
from ultralytics import YOLO


class YOLOv8PoseTracker:
    def __init__(self, model_path, font_path):
        self.model = YOLO(model_path)
        self.model_path = model_path
        self.font = font_path

        self.one_persons_track_id = None

        # настройки визуализации
        self.labels_color = (255, 255, 255)

    def predict_on_image(self, img):
        results = self.model(img)
        pred_class_idx = results[0].probs.top1
        pred_conf = results[0].probs.top1conf.item()
        pred_class_name = self.model.names[pred_class_idx]

        return pred_class_idx, pred_conf, pred_class_name

    def predict_on_image_and_viz(self, frame):
        results = self.model.track(frame, persist=True)
        annotated_frame = results[0].plot()

        return annotated_frame

    @staticmethod
    def _runOnVideo(video, maxFrames):
        """Генератор кадров из видео. Продолжает генерировать кадры
        пока не достигнуто количество кадров maxFrames.
        """

        readFrames = 0
        while True:
            hasFrame, frame = video.read()
            if not hasFrame:
                break

            yield frame

            readFrames += 1
            if readFrames > maxFrames:
                break

    @staticmethod
    def key_points_viz(
        image,
        results,
        text_color=(0, 0, 255),
        point_color=(0, 255, 0),
        bbox_color=(255, 0, 0),
    ):
        bbox_coords, keypoints, track_id = results

        # рисуем bbox
        if bbox_coords is not None:
            start_point = bbox_coords[:2].astype(int).tolist()
            end_point = bbox_coords[2:].astype(int).tolist()
            cv2.rectangle(image, start_point, end_point, bbox_color, thickness=2)

            # подписываем id-трека человека
            cv2.putText(
                image,
                f"track_id={track_id}",
                start_point,
                cv2.FONT_HERSHEY_SIMPLEX,
                1,
                text_color,
                2,
                cv2.LINE_AA,
            )

        if keypoints.sum() > 0:
            for i, xy in enumerate(keypoints):
                # рисуем ключевые точки на изображении
                x, y = int(xy[0]), int(xy[1])

                # если yolov8-pose не видит точку, то назначает ей координаты (0,0)
                # не отображаем такие точки
                if x > 0 and y > 0:
                    if i == 17:
                        color = (0, 255, 255)
                    else:
                        color = point_color

                    cv2.circle(image, (int(x), int(y)), 5, color, thickness=-1)

                    # подписываем индекс ключевой точки
                    cv2.putText(
                        image,
                        str(i),
                        (int(x), int(y)),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1,
                        text_color,
                        2,
                        cv2.LINE_AA,
                    )

        return image

    @staticmethod
    def get_means_without_zeros(matrix):
        matrix[np.where(matrix == 0)] = np.nan
        user_means = np.nanmean(matrix, axis=0)
        return user_means

    @staticmethod
    def euclidean_distance(point1, point2):
        return np.linalg.norm(point1 - point2)

    def get_one_person_from_track(self, results):
        if results[0].boxes.id is not None:
            keypoints = results[0].keypoints.xy.cpu().numpy()
            # проверяем у каких людей видны ключевые точки 6, 5, 12, 11
            # для двух людей видимость в кадре точек 6, 5, 12, 11 выглядит так:
            # array([[ True,  True,  True,  True],
            #        [ True,  True,  True,  True]])
            body_keypoints_vis = keypoints[:, [6, 5, 12, 11], :].mean(axis=2) > 0

            # получим индексы людей у которых видна хотя бы одна точка 5 или 6
            upper_body_vis = body_keypoints_vis[:, [0, 1]].sum(axis=1) > 0

            # получим индексы людей у которых видна хотя бы одна точка 12 или 11
            lower_body_vis = body_keypoints_vis[:, [2, 3]].sum(axis=1) > 0

            # отбираем таких людей
            good_idx = upper_body_vis * lower_body_vis

            track_ids = results[0].boxes.id.cpu().numpy()[good_idx]
            bbox_coords = results[0].boxes.xyxy.cpu().numpy()[good_idx]
            bbox_heights = bbox_coords[:, 3] - bbox_coords[:, 1]

            keypoints = keypoints[good_idx]
            points_cnt = np.count_nonzero(keypoints.sum(axis=2), axis=1)

            if len(bbox_coords):
                if self.one_persons_track_id in track_ids:
                    idx = np.where(track_ids == self.one_persons_track_id)[0][0]
                else:
                    # сортируем все объекты по высоте бокса и затем по количеству точек
                    objects = []
                    for idx, (bbox_height, points_cnt) in enumerate(
                        zip(bbox_heights, points_cnt)
                    ):
                        objects.append((idx, bbox_height, points_cnt))

                    objects.sort(key=lambda x: (-x[1], -x[2]))
                    idx = objects[0][0]
                    self.one_persons_track_id = int(track_ids[idx])

                # забираем лучшего кандидата
                keypoints = keypoints[idx]
                bbox_coords = bbox_coords[idx]
                center_point = self.get_means_without_zeros(keypoints[[5, 6, 11, 12]])
                center_point = center_point.reshape(1, -1)
                keypoints = np.concatenate((keypoints, center_point), axis=0)
            else:
                bbox_coords = None
                keypoints = np.zeros((18, 2))
        else:
            bbox_coords = None
            keypoints = np.zeros((18, 2))

        return bbox_coords, keypoints, self.one_persons_track_id

    def get_keypoints_features(self, keypoints):
        upper_point = self.get_means_without_zeros(keypoints[[5, 6]])
        lower_point = self.get_means_without_zeros(keypoints[[11, 12]])
        body_height = self.euclidean_distance(lower_point, upper_point)
        center_point = self.get_means_without_zeros(keypoints[[5, 6, 11, 12]])

        # вычисляем keypoints относительно центральной точки человека
        rows_flag = (keypoints.sum(axis=1) > 0).reshape(-1, 1)
        # нормируем keypoints на 5 высот тела человека
        keypoints_features = (keypoints - center_point) * rows_flag / (body_height * 5)

        return keypoints_features

    @staticmethod
    def get_uniform_frames(frames_num, uniform_frames_num):
        frames_idx = np.linspace(0, frames_num - 1, uniform_frames_num)
        frames_idx = np.round(frames_idx).astype(int)
        return frames_idx

    def predict_on_video_for_one_person(
        self, video_path, uniform_frames_num, fill_nan_to_vall=None
    ):
        # реинициализируем модель, чтобы треккинг работал корректно
        # для каждого нового видео
        # issue и ее решение
        # https://github.com/ultralytics/ultralytics/issues/8654#issuecomment-1978237058
        self.model = YOLO(self.model_path)

        video = cv2.VideoCapture(video_path)
        num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))

        # получаем нужное количество кадров, равноудаленных друг от друга
        uniform_frames_idx = self.get_uniform_frames(num_frames, uniform_frames_num)
        uniform_frames_idx = uniform_frames_idx.tolist()
        uniform_frames_idx = set(uniform_frames_idx)

        person_features = []

        # Enumerate the frames of the video
        video_frames_gen = self._runOnVideo(video, num_frames)
        frame_idx = 0
        for frame in tqdm.tqdm(video_frames_gen, total=num_frames):
            # для корректной работы треккинга берем каждый кадр
            results = self.model.track(frame, persist=True)

            # но признаки от keypoints забираем только с
            # равномерно распределенных кадров
            if frame_idx in uniform_frames_idx:
                results = self.get_one_person_from_track(results)

                bbox_coords, keypoints, track_id = results
                keypoints_features = self.get_keypoints_features(keypoints[:17])
                features_row = keypoints_features.flatten().tolist()
                person_features.append(features_row)
            else:
                pass

            frame_idx += 1

        video.release()
        person_features = np.array(person_features)
        person_features[person_features == -0] = 0

        if fill_nan_to_vall is not None:
            person_features[np.isnan(person_features)] = fill_nan_to_vall

        return person_features

    def predict_on_video_and_viz(self, video_path, path_out_video, one_person=False):
        # реинициализируем модель, чтобы треккинг работал корректно
        # для каждого нового видео
        # issue и ее решение
        # https://github.com/ultralytics/ultralytics/issues/8654#issuecomment-1978237058
        self.model = YOLO(self.model_path)

        video = cv2.VideoCapture(video_path)
        width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        frames_per_second = video.get(cv2.CAP_PROP_FPS)

        # Initialize video writer
        fourcc = cv2.VideoWriter_fourcc(*"MJPG")

        video_writer = cv2.VideoWriter(
            path_out_video,
            fourcc=fourcc,
            fps=float(frames_per_second),
            frameSize=(width, height),
            isColor=True,
        )

        num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))

        # Enumerate the frames of the video
        video_frames_gen = self._runOnVideo(video, num_frames)
        for frame in tqdm.tqdm(video_frames_gen, total=num_frames):
            # Run YOLOv8 inference on the frame
            results = self.model.track(frame, persist=True)
            if one_person:
                results = self.get_one_person_from_track(results)

            # Visualize the results on the frame
            if not one_person:
                annotated_frame = results[0].plot()
            else:
                annotated_frame = self.key_points_viz(frame, results)

            # Write to video file
            video_writer.write(annotated_frame)

        video.release()
        video_writer.release()


if __name__ == "__main__":
    image_path = "data/processed/kinetics_700_2020_cls/train/swing dancing/video__1e38sSEhi4_frame_15.jpg"
    output_images_dir = "data/output/pose_estimation/output_images"

    input_videos_dir = "data/interim/kinetics_700_2020/train/videos/"
    videos_names = [
        # хотим чтобы на этих видео работало хорошо
        "video___KgrOyZBlQ.mp4",
        "video__1e38sSEhi4.mp4",
        "video__1tHSvXN7BM.mp4",
        "video__6d4WxnukRU.mp4",
        "video__39KVssPNP4.mp4",
        "video__fGEM_oExVU.mp4",
        "video__GQvowFzEgA.mp4",
        "video_xihIy0IQn9I.mp4",
        "video_ZGwhXRnMdAA.mp4",
        # сложные видео-вызовы, корректная работа не гарантируется
        "video__5y6PFTHyP4.mp4",
        "video__6PYt-fmMto.mp4",
        "video_10oKh9lCEIQ.mp4",
    ]
    # videos_names = ["video__1e38sSEhi4.mp4"]
    output_videos_dir = "data/output/pose_estimation/output_videos"

    model_font_path = "models/fonts/PTSans-Regular.ttf"
    model_path = "yolov8m-pose.pt"
    pose_tracker = YOLOv8PoseTracker(model_path, model_font_path)

    # создаем папки для хранения визуализаций
    os.makedirs(output_images_dir, exist_ok=True)
    os.makedirs(output_videos_dir, exist_ok=True)

    # Предсказать ключевые точки всех людей на картинке и визуализировать skeletons
    image = pose_tracker.predict_on_image_and_viz(image_path)
    image_name = os.path.split(image_path)[-1]
    image_output_path = os.path.join(output_images_dir, image_name)
    cv2.imwrite(image_output_path, image)

    # Предсказать ключевые точки всех людей на видео и визуализировать skeletons
    for video_name in videos_names:
        video_path = os.path.join(input_videos_dir, video_name)
        video_output_path = os.path.join(output_videos_dir, video_name)
        pose_tracker.predict_on_video_and_viz(
            video_path, video_output_path, one_person=True
        )
        print(f"Save predictions {video_output_path}")
