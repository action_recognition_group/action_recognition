import numpy as np
import pandas as pd
import torch
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from torch import nn
from torch.utils.data import Dataset


def read_dataset(path_to_csv):
    df = pd.read_csv(path_to_csv, sep=";")

    videos = df["video_path"].unique()

    data, labels = [], []
    for video in videos:
        df_video = df[df["video_path"] == video].reset_index()
        label_video = df_video["label_id"][0]
        df_video = df_video.drop(["label", "label_id", "video_path", "index"], axis=1)
        data_video = df_video.to_numpy()

        data.append(data_video.T)
        labels.append(label_video)

    data = torch.tensor(data, dtype=torch.float32)
    labels = torch.tensor(labels, dtype=torch.long)

    return data, labels


class TimeSeriesDataset(Dataset):
    def __init__(self, data, labels):
        self.data = data
        self.labels = labels

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data[idx], self.labels[idx]


# Positional Encoding for Transformer
class PositionalEncoding(nn.Module):
    # https://pytorch.org/tutorials/beginner/transformer_tutorial.html
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(
            torch.arange(0, d_model, 2).float() * (-np.log(10000.0) / d_model)
        )
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer("pe", pe)

    def forward(self, x):
        x = x + self.pe[: x.size(0), :]
        return self.dropout(x)


# Model definition using Transformer
class TransformerModel(nn.Module):
    def __init__(self, input_dim, d_model, nhead, num_layers, num_classes, dropout=0.2):
        super(TransformerModel, self).__init__()
        """
        Класс модели трансформера для классификации последовательностей.

        Parameters
        ----------
        input_dim : int
            Количество ожидаемых признаков в входных данных кодировщика/декодера.
            Это размерность входных векторов, с которыми работает трансформер.
            Параметр соответствует размерности векторов, которые представляют слова
            или токены в тексте.
        d_model : int
            Максимальная длина последовательности, которую может обрабатывать модель.
            Задает размерность входного embedding layer и для positional encoder.
        nhead : int
            Количество heads в multihead self-attention слоях модели.
        num_layers : int
            Количество слоев в кодировщике и декодере.
            Параметр определяет глубину сети (количество слоев).
        num_classes: int
            Количество классов для классификации.
        dropout: float
            Маскирование позиционных эмбедингов.
        """
        self.encoder = nn.Linear(input_dim, d_model)
        self.pos_encoder = PositionalEncoding(d_model, dropout)
        encoder_layers = nn.TransformerEncoderLayer(d_model, nhead)
        self.transformer_encoder = nn.TransformerEncoder(encoder_layers, num_layers)
        self.decoder = nn.Linear(d_model, num_classes)

    def forward(self, x):
        x = self.encoder(x)
        x = self.pos_encoder(x)
        x = self.transformer_encoder(x)
        x = self.decoder(x[:, -1, :])
        return x


class LSTMClassifier(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, num_classes):
        super(LSTMClassifier, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.lstm = nn.LSTM(
            input_size,
            hidden_size,
            num_layers,
            batch_first=True,
            bidirectional=True,
            dropout=0.1,
        )
        # Multiply by 2 for bidirectional
        self.fc = nn.Linear(hidden_size * 2, num_classes)

    def forward(self, x):
        # Multiply by 2 for bidirectional
        h0 = torch.zeros(self.num_layers * 2, x.size(0), self.hidden_size).to(x.device)
        c0 = torch.zeros(self.num_layers * 2, x.size(0), self.hidden_size).to(x.device)

        out, _ = self.lstm(x, (h0, c0))

        out = self.fc(out[:, -1, :])
        return out


def predict_model(model, test_loader, device):
    model.eval()
    with torch.no_grad():
        preds, labels = [], []
        for data, target in test_loader:
            output = model(data.to(device))
            _, predicted = torch.max(output.data, 1)
            preds.extend(predicted.cpu().tolist())
            labels.extend(target.cpu().tolist())

        preds = np.array(preds)
        labels = np.array(labels)

    return preds, labels


def evaluate_metrics(preds, targets):
    # считаем метрики
    acc = accuracy_score(targets, preds)
    recall = recall_score(targets, preds, average="macro")
    precision = precision_score(targets, preds, average="macro")
    f1 = f1_score(targets, preds, average="macro")

    return acc, recall, precision, f1
