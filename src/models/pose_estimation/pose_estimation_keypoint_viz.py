import os

import cv2
from ultralytics import YOLO


def key_points_viz(
    image, results, threshold, text_color=(0, 0, 255), point_color=(0, 255, 0)
):
    keypoints = results[0].keypoints.xy.cpu().numpy()[0]
    conf = results[0].keypoints.conf.cpu().numpy()[0]

    for i, (xy, conf) in enumerate(zip(keypoints, conf)):
        if conf > threshold:
            # рисуем ключевые точки на изображении
            x, y = int(xy[0]), int(xy[1])
            cv2.circle(image, (int(x), int(y)), 5, point_color, thickness=-1)

            # подписываем индекс ключевой точки
            cv2.putText(
                image,
                str(i),
                (int(x), int(y)),
                cv2.FONT_HERSHEY_SIMPLEX,
                1,
                text_color,
                2,
                cv2.LINE_AA,
            )

    return image


if __name__ == "__main__":
    image_path = "data/external/man_in_the_suit.jpg"
    model_path = "yolov8m-pose.pt"
    threshold = 0.6
    output_images_dir = "data/output/pose_estimation/keypoints_map"
    output_image_name = "keypoints_map.jpg"
    
    os.makedirs(output_images_dir, exist_ok=True)

    model = YOLO(model_path)
    image = cv2.imread(image_path)

    results = model(image_path)
    image = key_points_viz(image, results, threshold)

    output_image_path = os.path.join(output_images_dir, output_image_name)
    cv2.imwrite(output_image_path, image)
