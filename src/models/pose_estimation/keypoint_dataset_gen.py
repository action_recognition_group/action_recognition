import json
import os

import numpy as np
import pandas as pd
from pose_tracker_inference import YOLOv8PoseTracker


def read_json(json_path):
    with open(json_path, "r") as file:
        data = json.load(file)

    return data


def extend_matrix_by_zeros(matrix, wanted_shape):
    # Create an array of zeros with wanted_shape
    zeros_shape = wanted_shape[0], wanted_shape[1] - matrix.shape[1]
    zeros_to_append = np.zeros((zeros_shape))

    matrix = np.concatenate((matrix, zeros_to_append), axis=1)

    return matrix


if __name__ == "__main__":
    input_dataset_path = (
        "data/interim/kinetics_700_2020/val/val_above_fps12.csv"
    )
    output_dataset_path = "data/processed/pose_estimation/keypoint_features_test.csv"

    model_font_path = "models/fonts/PTSans-Regular.ttf"
    model_path = "yolov8m-pose.pt"
    pose_tracker = YOLOv8PoseTracker(model_path, model_font_path)

    names_to_ids_json_path = "src/models/frame_classifier/names_to_ids.json"

    keypoints_num = 17
    # согласно анализу fps в видео
    # см. notebooks/eda_new_dataset_download.ipynb
    # мы выбрали подвыборки (примерно 96-97% от исходной)
    # где FPS >= 12.
    # Продолжительность видео 10 сек, итого берем 120
    # равномерно распределенных по всей длине видео кадров.
    frames_num = 120

    wanted_shape = (keypoints_num * 2, frames_num)

    # reads classes names to ids mapping
    names_to_ids = read_json(names_to_ids_json_path)

    df_src = pd.read_csv(input_dataset_path)

    # создадим DataFrame для хранения датасета ключевых точек
    column_names = []
    column_names.extend(["label", "label_id", "video_path"])
    feature_columns = []
    for i in range(frames_num):
        feature_columns.append(f"frame_{i}")

    column_names.extend(feature_columns)

    df = pd.DataFrame(columns=column_names)

    # Предсказать ключевые точки для одного человека на видео и визуализировать skeletons
    for index, row in df_src.iterrows():
        video_path = row.name_video
        label = row.label
        person_features = pose_tracker.predict_on_video_for_one_person(
            video_path, frames_num, fill_nan_to_vall=0
        )

        df_video = pd.DataFrame(person_features.T, columns=feature_columns)
        df_video["label"] = [label] * len(df_video)
        df_video["label_id"] = [names_to_ids[label]] * len(df_video)
        df_video["video_path"] = [video_path] * len(df_video)

        df = pd.concat([df, df_video], axis=0, ignore_index=True)

    datasets_dir = os.path.split(output_dataset_path)[0]
    os.makedirs(datasets_dir, exist_ok=True)

    df.to_csv(output_dataset_path, index=False, sep=";")
    print(f"Save person features {output_dataset_path}")
