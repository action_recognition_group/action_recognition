import os

import pandas as pd
from sklearn.model_selection import train_test_split


def save_subset(df_train, dataset_dir, df_train_name):
    df_train_path = os.path.join(dataset_dir, df_train_name)
    df_train.to_csv(df_train_path, index=False)


if __name__ == "__main__":
    dataset_csv_path = "data/interim/kinetics_700_2020/train/train.csv"

    df = pd.read_csv(dataset_csv_path)
    df_train, df_val = train_test_split(
        df, test_size=0.2, stratify=df.label, random_state=42
    )

    dataset_dir = os.path.split(dataset_csv_path)[0]

    save_subset(df_train, dataset_dir, "train_part.csv")
    save_subset(df_val, dataset_dir, "val_part.csv")
