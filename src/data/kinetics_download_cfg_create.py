import json


if __name__ == "__main__":
    subset = "train"

    cfg_dct = {
        "dataset_name": "kinetics_700_2020",
        "subset": subset,
        # ищем все классы в датасете, содержащие в качестве подстроки данное слово
        "search_word": "dancing",
        "dataset_csv_path": f"data/raw/kinetics_700_2020/{subset}.csv",
        "videos_per_class": 200,
        "dataset_dest_dir": "data/interim",
        "resume_dataset_dir": "",
        "resume_csv_name": "",
        "resume_log_file_name": "",
    }

    cfg_json_path = f"src/data/download_cfg/cfg_{subset}.json"

    with open(cfg_json_path, "w") as outfile:
        json.dump(cfg_dct, outfile, indent=4)
