import json


if __name__ == "__main__":
    subset = 'train'
    
    cfg_dct = {
        "dataset_name": "kinetics_700_2020_cls",
        "subset": subset,
        "csv_path": "data/interim/kinetics_700_2020/train/train_part.csv",
        "frame_rate": 5,
        "dataset_dest_dir": "data/processed",
    }

    cfg_json_path = f"src/data/slice_videos_cfg/cfg_{subset}.json"

    with open(cfg_json_path, "w") as outfile:
        json.dump(cfg_dct, outfile, indent=4)
