from argparse import ArgumentParser
import os

import cv2
import pandas as pd

from kinetics_download import load_cfg


def slice_video_for_frames(video_path, dir_path, frame_rate, dim=None):
    """Функция разрезает видео по пути video_path.
    Функция создает в той же директории, что и видео папку с таким же названием,
    где будет хранить кадры из видео в виде картинок.

    Parameters
    ----------
    video_path : str
        полный путь к видео которое нужно порезать на кадры.
    dir_path : str
        директория куда будем сохранять кадры из видео.
    """
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    vidcap = cv2.VideoCapture(video_path)
    fps = vidcap.get(cv2.CAP_PROP_FPS)
    print(f"read video {video_path}, FPS is {fps}")

    success, image = vidcap.read()
    count = 0

    video_name = os.path.split(video_path)[-1]
    video_name = os.path.splitext(video_name)[0]

    while success:
        if count % frame_rate == 0:
            frame_path = f"{video_name}_frame_{count}.jpg"
            frame_path = os.path.join(dir_path, frame_path)

            if dim is not None:
                image = cv2.resize(image, dim, interpolation=cv2.INTER_NEAREST)

            res = cv2.imwrite(frame_path, image)

            if res:
                print(frame_path)
            else:
                raise ValueError("Изображение не было сохранено !")

        count += 1
        success, image = vidcap.read()


def slice_dataset_videos_for_frames(
    dataset_dest_dir, dataset_name, subset, csv_path, frame_rate
):
    dataset_dest_dir = os.path.join(dataset_dest_dir, dataset_name, subset)
    os.makedirs(dataset_dest_dir, exist_ok=True)

    df = pd.read_csv(csv_path)

    for index, row in df.iterrows():
        video_dir_path = os.path.join(dataset_dest_dir, row.label)
        slice_video_for_frames(row.name_video, video_dir_path, frame_rate)


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Kinetics pictures dataset creating args parser"
    )

    parser.add_argument(
        "--cfg",
        type=str,
        help="Path to json file with Kinetics pictures dataset creating parameters",
    )

    args = parser.parse_args()
    cfg = load_cfg(args.cfg)

    slice_dataset_videos_for_frames(**cfg)
