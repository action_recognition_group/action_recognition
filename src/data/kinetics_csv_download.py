import os
import wget


if __name__ == "__main__":
    # kinetics train.csv, val.csv, test.csv links from main git repo
    # https://github.com/cvdfoundation/kinetics-dataset
    dataset_name = "kinetics_700_2020"
    main_dataset_url = "https://s3.amazonaws.com/kinetics/700_2020/annotations"
    train_part = "train.csv"
    val_part = "val.csv"
    test_part = "test.csv"
    dataset_dest_dir = "data/raw"
    
    dataset_dest_dir = os.path.join(dataset_dest_dir, dataset_name)
    os.makedirs(dataset_dest_dir, exist_ok=True)

    for subset in [train_part, val_part, test_part]:
        subset_url = os.path.join(main_dataset_url, subset)
        subset_csv_path = os.path.join(dataset_dest_dir, subset)
        print(f"Downloading {dataset_name} {subset}...")
        wget.download(subset_url, subset_csv_path)
        print(f"Download {subset} to {subset_csv_path}")
