import json
import logging
import os
from argparse import ArgumentParser
from datetime import datetime

import pandas as pd
import pytube
from moviepy.video.io.VideoFileClip import VideoFileClip
from pytube.exceptions import PytubeError


def create_logger(logger_name, log_file_path, mode="w"):
    """Create a logger with a file handler.

    This function sets up a logger with a file handler that formats log messages
    to include the timestamp and the log message itself. The logger
    is configured to log messages with a severity level of INFO or higher.

    Parameters
    ----------
    logger_name : str
        The name of the logger. This is used to retrieve or create a logger
        instance.
    log_file_path : str
        The path to the log file where the log messages will be written.

    Returns
    -------
    logger : logging.Logger
        A logger instance configured with the specified name and file handler.
    """
    logger = logging.getLogger(logger_name)

    file_handler = logging.FileHandler(log_file_path, mode)
    formatter = logging.Formatter("%(asctime)s - %(message)s")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    logger.setLevel(logging.INFO)
    return logger


def get_subset_of_classes(dataset_csv_path, search_word, videos_per_class):
    data = pd.read_csv(dataset_csv_path)
    df = data[data.label.str.contains(search_word)]

    df_subset = pd.DataFrame()
    label_values = df["label"].unique()

    for label in label_values:
        df_label = df[df["label"] == label]
        videos_per_class = min(videos_per_class, len(df_label))
        label_data = df[df["label"] == label].sample(videos_per_class)
        df_subset = pd.concat([df_subset, label_data])

    df_subset = df_subset.reset_index()

    length = df_subset.shape[0]
    df_subset["name_video"] = [None] * length

    return df_subset


def download_from_youtube(video_url, video_path):
    yt = pytube.YouTube(video_url)

    # Filter the streams to get only mp4 files
    stream = yt.streams.filter(file_extension="mp4").get_highest_resolution()

    stream.download(filename=video_path)


def cut_video(video_path, start_time, end_time, name_video):
    clip = VideoFileClip(video_path).without_audio()
    clip = clip.subclip(start_time, end_time)
    clip.write_videofile(name_video)
    duration = clip.duration
    return duration


def download_dataset(
    dataset_csv_path,
    dataset_name,
    subset,
    dataset_dest_dir,
    search_word,
    videos_per_class,
    resume_dataset_dir=None,
    resume_csv_name=None,
    resume_log_file_name=None,
):
    logger_name = f"{dataset_name} download logger"

    if resume_dataset_dir and resume_csv_name and resume_log_file_name:
        # continue previous dataset downloading
        dataset_dest_dir = resume_dataset_dir
        resume_csv_path = os.path.join(dataset_dest_dir, resume_csv_name)
        resume_log_file_path = os.path.join(dataset_dest_dir, resume_log_file_name)

        logger = create_logger(logger_name, resume_log_file_path, "a")

        df = pd.read_csv(resume_csv_path)
        start_row_idx = df["name_video"].dropna().index[-1] + 1

        message = f"Continue from {start_row_idx} ..."
        print(message)
        logger.info(message)

    else:
        # create dir for dataset's data
        dataset_dest_dir = os.path.join(dataset_dest_dir, dataset_name, subset)
        os.makedirs(dataset_dest_dir, exist_ok=True)

        # create a logger to log train process
        start_time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        log_file_path = f"kinetics_download_log_{start_time}.log"
        log_file_path = os.path.join(dataset_dest_dir, log_file_path)
        logger = create_logger(logger_name, log_file_path)

        message = f"Creating catalog to store dataset {dataset_dest_dir}"
        print(message)
        logger.info(message)

        df = get_subset_of_classes(dataset_csv_path, search_word, videos_per_class)
        start_row_idx = 0

    video_dir = "videos"
    video_dir = os.path.join(dataset_dest_dir, video_dir)
    os.makedirs(video_dir, exist_ok=True)

    for i in range(start_row_idx, len(df)):
        try:
            row = df.iloc[i]
            tag = row["youtube_id"]
            start_time = row.time_start
            end_time = row.time_end

            video_url = f"https://www.youtube.com/watch?v={tag}"
            video_name = "tmp_video.mp4"
            video_path = os.path.join(dataset_dest_dir, video_name)
            download_from_youtube(video_url, video_path)

            subclip_path = f"video_{tag}.mp4"
            subclip_path = os.path.join(video_dir, subclip_path)
            duration = cut_video(video_path, start_time, end_time, subclip_path)

            if duration != end_time - start_time:
                message = f"Incorrect clip duration for {tag}"
                logger.error(message)
                raise ValueError(message)

            message = f"Download {video_url} clip duration {duration} seconds clip=({start_time}, {end_time})"
            logger.info(message)
            print(message)

            os.remove(video_path)
            df["name_video"][i] = subclip_path
            dataset_csv_name = f"{subset}_i_last.csv"
            dataset_csv_path = os.path.join(dataset_dest_dir, dataset_csv_name)
            df.to_csv(dataset_csv_path, index=False)

        except PytubeError as e:
            message = f"Error occurred while processing video {i} {tag}: {str(e)}"
            print(message)
            logger.error(message)
        except Exception as e:
            message = (
                f"Unexpected error occurred while processing video {i} {tag}: {str(e)}"
            )
            print(message)
            logger.error(message)

        message = f"{i} / {len(df) - 1} is ready"
        print(message)
        logger.info(message)

    # result dataset post processing
    df.dropna(inplace=True)
    df.drop(
        ["index", "youtube_id", "time_start", "time_end", "split"], axis=1, inplace=True
    )

    dataset_csv_name = f"{subset}.csv"
    dataset_csv_path = os.path.join(dataset_dest_dir, dataset_csv_name)
    df.to_csv(dataset_csv_path, index=False)
    message = f"Saving dataset to {dataset_csv_path}"
    print(message)
    logger.info(message)


def load_cfg(json_cfg_path):
    with open(json_cfg_path, "r") as file:
        cfg = json.load(file)

    return cfg


if __name__ == "__main__":
    parser = ArgumentParser(description="Kinetics dataset downloading args parser")

    parser.add_argument(
        "--cfg",
        type=str,
        help="Path to json file with Kinetics dataset downloading parameters",
    )

    args = parser.parse_args()
    cfg = load_cfg(args.cfg)

    download_dataset(**cfg)
